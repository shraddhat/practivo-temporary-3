/*
***************************************
 class name-DatabaseQueries
 Class Description-Class which contains methods for generating SOQL querries for all th objects.
 *************************************
*/
public with sharing class DatabaseQueries {
    
    public static Integer serviceProviderCode=1;
    public static Integer customerCode=2;
    public static Integer assistantCode=3;
    
    public  static string contactFields = ' ID,Name,Email,firstname,lastname,Phone,Birthdate,RecordTypeId,PractitionerColor__c,Salutation,Start_Date_To_Generate_Calendar__c,End_Date_To_Generate_Calendar__c,Resource_Type__r.Id';
    
    public  static string eventFields = '   Id,Subject,Capacity__c, IsAllDayEvent,StartDateTime, EndDateTime, WhoId, WhatId, Appointment_Type__c, Appointment_Type__r.Maximum_Participant__c , Description__c, Event_Type__c, Location_Category_Mapping__c, No_of_Participants__c';
    
    public  static string locationCategoryMapFields = ' Id,Name,Category__r.Id,Category__r.Name,contact__r.RecordType.DeveloperName,Contact__r.Name,Contact__r.Salutation,Contact__r.Id,Location__r.Name,Location__r.Id,Contact__r.PractitionerColor__c,Contact__r.Resource_Type__r.Id      ';

    public  static string appointmentTypeFields = ' Id,Name,Category__c ,DefaultDuration__c,Maximum_Participant__c,Price__c,Category__r.Name,Description__c';

    public  static string ActualResourceBookedFileds = '    Id,Name,Assistants__c,Booked_Resources__c,Resources__r.ID,doNotOverbook__c,Resources__c ';

    public  static string appTypResTypMappingFields = 'Id,Appointment_Type__c,Appointment_Type__r.Id ,isAssistantRequired__c, Appointment_Type__r.DefaultDuration__c , Appointment_Type__r.Maximum_Participant__c,isSpecific__c,No_of_Resources_Required__c,Resource_Type__c,Resource_Type__r.Type_of_Resource__c,Resource_Type__r.Id,Specific_Resource_Id__c,isHumanResource__c';

    public  static string LocationFields ='Id,Name,Address__c';

    public  static string CategoryFields ='Id,Name';

    public  static String Resourcetypefields='Id,Name,Type_of_Resource__c,Description__c,Total_No_of_Resources__c,Total_Overbooking_Limit__c';

    public  static String ResourceFields='Id,Name,Location__c,Resource_Type__c,Resource_Type__r.Type_of_Resource__c,ResourceColor__c,Over_Booking_Limit__c,Description__c,Resource_Type__r.Id,Location__r.Id';

    private static string formLocationQuery(string condition)
    {
        string queryString;
        queryString = 'SELECT ' + LocationFields +' FROM Location__c ' + condition + ' LIMIT 2000';
        return queryString;
    }
    private static string formCategoryQuery(string condition)
    {
        string queryString;
        queryString = 'SELECT ' + CategoryFields +' FROM Category__c ' + condition + ' LIMIT 2000';
        return queryString;
    }
    private static string formAppTyResTypQuery(string condition)
    {
        string queryString;
        queryString = 'SELECT ' + appTypResTypMappingFields +' FROM Resource_Type_Appointment_Type_Mapping__c ' + condition + ' LIMIT 2000';
        return queryString;
    }
    private static string formAppointmentTypeQuery(string condition)
    {
        string queryString;
        queryString = 'SELECT ' + appointmentTypeFields +' FROM AppointmentType__c ' + condition + ' LIMIT 2000';
        return queryString;
    }
    private static string formLocationCayegoryMapQuery(string Condition)
    {
        string queryString;
        queryString = 'SELECT ' + locationCategoryMapFields +' FROM Location_Category_Mapping__c ' + condition + ' LIMIT 2000';
        return queryString;
    }
    private static string formEventQuery(string condition)
    {
        string queryString;
        queryString = 'SELECT ' + eventFields +' FROM EVENT ' + condition + ' LIMIT 2000';
        return queryString;
    }
    private static string formResourceTypeQuery(string condition)
    {
        string queryString;
        queryString = 'SELECT ' + Resourcetypefields +' FROM Resource_Type__c ' + condition + ' LIMIT 2000';
        return queryString;
    }
    private static string formResourceQuery(string condition)
    {
        string queryString;
        queryString = 'SELECT ' + ResourceFields +' FROM Resources__c ' + condition + ' LIMIT 2000';
        return queryString;
    }
    private static string formActualResourceBookedMapQuery(string condition)
    {
        string queryString;
        queryString = 'SELECT ' + ActualResourceBookedFileds +' FROM Actual_Resource_Booked_Mapping__c ' + condition + ' LIMIT 2000';
        return queryString;
    }
    private static string formContactQuery(string condition,integer value)
    {
        string queryString;
        if(value==serviceProviderCode)
        {
            queryString =  'SELECT' + contactFields +'  FROM Contact    ' + condition + ' LIMIT 2000';      
        }
        if(value==customerCode)
        {
            queryString =  'SELECT' + contactFields +'  FROM Contact    ' + condition + ' LIMIT 2000';      
        }
        return queryString;
    }
    
    public static list<Location__c> getLocationData(string condition)
    {
        list<Location__c> getLocationLst;
        try
        {
            string queryString = formLocationQuery(condition);
            system.debug('==> DatabaseQueries:formLocationQuery:queryString ==>' + queryString);
            if(Utils.getReadAccessCheck(Utils.PackageNamespace+'Location__c',new String []{'Id','Name','address__c'}))
                getLocationLst= Database.query(queryString);
            else
                throw new Utils.ParsingException('No Read access to Location__c or Fields in Location__c.');
        }
        catch(Exception e)
        {
            System.debug('DatabaseQueries-->getLocationData-->'+e.getstacktracestring());
        }        
        return getLocationLst;
    }
    public static list<Actual_Resource_Booked_Mapping__c> getActualResourceBookedMapData(string condition)
    {
        list<Actual_Resource_Booked_Mapping__c> getActualResourceBookedMapLst;
        try
        {
            string queryString = formActualResourceBookedMapQuery(condition);
            system.debug('==> DatabaseQueries:formActualResourceBookedMapQuery:queryString ==>' + queryString);
            if(Utils.getReadAccessCheck(Utils.PackageNamespace+'Actual_Resource_Booked_Mapping__c',new String []{'Id','Name','Assistants__c','Booked_Resources__c','Resources__c','doNotOverbook__c'}))        
                getActualResourceBookedMapLst= Database.query(queryString);
            else
               throw new Utils.ParsingException('No Read access to Actual_Resource_Booked_Mapping__c or Fields in Actual_Resource_Booked_Mapping__c.');
        }
        catch(Exception e)
        {
            System.debug('DatabaseQueries-->getActualResourceBookedMapData-->'+e.getstacktracestring());
        }         
        return getActualResourceBookedMapLst;
    }
    public static list<Category__c> getCategoryData(string condition)
    {
        list<Category__c> getCategoryLst;
        try
        {
            string queryString = formCategoryQuery(condition);
            system.debug('==> DatabaseQueries:formCategoryQuery:queryString ==>' + queryString);
            if(Utils.getReadAccessCheck(Utils.PackageNamespace+'Category__c',new String []{'Id','Name'}))
                getCategoryLst= Database.query(queryString);
            else
                   throw new Utils.ParsingException('No Read access to Category__c or Fields in Category__c.');
        }
        catch(exception e)
        {
           System.debug('DatabaseQueries-->getCategoryData-->'+e.getstacktracestring()); 
        }               
        return getCategoryLst;
    }
    public static list<Location_Category_Mapping__c> getLocCatMapping(string condition)
    {
        list<Location_Category_Mapping__c> getLocCatMappingLst;
        try
        {
            string queryString = formLocationCayegoryMapQuery(condition);
            system.debug('==> DatabaseQueries:formLocationCayegoryMapQuery:queryString ==>' + queryString);
            if(Utils.getReadAccessCheck(Utils.PackageNamespace+'Location_Category_Mapping__c',new String []{'Id','Contact__c','Name','Location__c','Category__c'}))
                getLocCatMappingLst= Database.query(queryString);
             else
                throw new Utils.ParsingException('No Read access to Location_Category_Mapping__c or Fields in Location_Category_Mapping__c.'); 
        }
        catch(Exception e)
        {
            System.debug('DatabaseQueries-->getLocCatMapping-->'+e.getstacktracestring());
        }           
        return getLocCatMappingLst;
    }
    public static list<Event> getEventData(string condition,date startDate,date endDate)
    {
        list<Event> eventLst;
        try
        {
            string queryString = formEventQuery(condition);
            system.debug('==> DatabaseQueries:getEventData:queryString ==>' + queryString);
            if(Utils.getReadAccessCheck('Event',new String []{'Id','Subject','Capacity__c', 'StartDateTime', 'EndDateTime', 'WhoId', 'WhatId', 'Appointment_Type__c',
                              'Description__c','Event_Type__c','Location_Category_Mapping__c','No_of_Participants__c','IsAllDayEvent'}))
                eventLst= Database.query(queryString);
            else
                throw new Utils.ParsingException('No Read access to Event or Fields in Event.');
        }
        catch(Exception e)
        {
            System.debug('DatabaseQueries-->getEventData-->'+e.getstacktracestring());
        }        
        return eventLst;
    }
    public static list<Resource_Type__c> getResourceTypeData(string condition)
    {
        list<Resource_Type__c> getResourceTypeLst;
        try
        {
            string queryString = formResourceTypeQuery(condition);
            system.debug('==> DatabaseQueries:formResourceTypeQuery:queryString ==>' + queryString);
            if(Utils.getReadAccessCheck(Utils.PackageNamespace+'Resource_Type__c',new String []{'Id','Name','Type_of_Resource__c','Description__c','Total_No_of_Resources__c','Total_Overbooking_Limit__c'}))
                getResourceTypeLst= Database.query(queryString);
            else
                throw new Utils.ParsingException('No Read access to Resource_Type__c or Fields in Resource_Type__c.');
        }
        catch(Exception e)
        {
            System.debug('DatabaseQueries-->getResourceTypeData-->'+e.getstacktracestring());
        }                
        return getResourceTypeLst;
    }
    public static list<Resources__c> getResourceData(string condition)
    {
        list<Resources__c> getResourceLst;
        try
        {
            string queryString = formResourceQuery(condition);
            system.debug('==> DatabaseQueries:formResourceQuery:queryString ==>' + queryString);
            if(Utils.getReadAccessCheck(Utils.PackageNamespace+'Resources__c',new String []{'Id','Name','Location__c','Description__c','Over_Booking_Limit__c','ResourceColor__c','Resource_Type__c'}))
                getResourceLst= Database.query(queryString);
            else
                throw new Utils.ParsingException('No Read access to Resources__c or Fields in Resources__c.');
        }
        catch(Exception e)
        {
            System.debug('DatabaseQueries-->getResourceData-->'+e.getstacktracestring());
        }        
        return getResourceLst;
    }
    public static list<Contact> getContactData(string condition,integer code)
    {
        list<Contact> contactLst;
        try
        {
            string queryString = formContactQuery(condition,code);
            system.debug('==> DatabaseQueries:getContactData:queryString ==>'+queryString);
            if(Utils.getReadAccessCheck('contact',new String []{'Id','name','Phone','firstname','lastname','Birthdate','Email','Salutation','RecordTypeId','PractitionerColor__c','Start_Date_To_Generate_Calendar__c','End_Date_To_Generate_Calendar__c'}))
                contactLst= Database.Query(queryString);
            else
                throw new Utils.ParsingException('No Read access to contact or Fields in contact.');
        }
        catch(exception e)
        {
            System.debug('DatabaseQueries-->getContactData-->'+e.getstacktracestring());
        }            
        return contactLst;
    }
    public static list<AppointmentType__c> getAppointmentTypeData(string condition)
    {
        list<AppointmentType__c> appointmentTypeLst;
        try
        {
            string queryString = formAppointmentTypeQuery(condition);
            system.debug('==> DatabaseQueries:getAppointmentTypeData:queryString ==>'+queryString);
            if(Utils.getReadAccessCheck(Utils.PackageNamespace+'AppointmentType__c',new String []{'Id','Name','Category__c','DefaultDuration__c','Maximum_Participant__c','Price__c','Description__c'}))
                appointmentTypeLst = Database.Query(queryString);
            else
                throw new Utils.ParsingException('No Read access to AppointmentType__c or Fields in AppointmentType__c.');    
        }
        catch(Exception e)
        {
            System.debug('DatabaseQueries-->getAppointmentTypeData-->'+e.getstacktracestring());
        }        
        return appointmentTypeLst;
    }
    public static list<Resource_Type_Appointment_Type_Mapping__c> getAppTypResTypMapping(string condition)
    {
        list<Resource_Type_Appointment_Type_Mapping__c> lst;
        try
        {
            string queryString = formAppTyResTypQuery(condition);
            system.debug('==> DatabaseQueries:getAppTypResTypMapping:queryString ==>'+queryString);
            if(Utils.getReadAccessCheck(Utils.PackageNamespace+'Resource_Type_Appointment_Type_Mapping__c',new String []{'Id','Appointment_Type__c','isSpecific__c','No_of_Resources_Required__c','Resource_Type__c','Specific_Resource_Id__c','isHumanResource__c','isAssistantRequired__c'}))
                lst= Database.Query(queryString);
            else
                throw new Utils.ParsingException('No Read access to Resource_Type_Appointment_Type_Mapping__c or Fields in Resource_Type_Appointment_Type_Mapping__c.');
        }
        catch(Exception e)
        {
            System.debug('DatabaseQueries-->getAppTypResTypMapping-->'+e.getstacktracestring());
        }        
        return lst;
    }
}