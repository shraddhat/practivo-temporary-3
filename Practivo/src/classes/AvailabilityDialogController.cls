public class AvailabilityDialogController
{/*
    public Appointment__c fakeAppointment {get; set;}
    public Id selectedPractitionerId {get;set;}
    public List<SelectOption> availabilityTypeOptions {get;set;}
    public String availabilityType {get;set;}
    public List<Boolean> weekDaysRegular {get;set;}
    public Map<Integer, List<WorkingTimeSlot__c>> timeSlotMap {get;set;}
    public List<Integer> weekDays{get;set;}
    public List<WorkingTimeSlot__c> regularTS {get;set;}
    public Map<Integer, String> weekDaysNames{get;set;}
    private List<WorkingTimeSlot__c> slotsToDelete;
    public Boolean isSavedSuccessfully {get;set;}
    
    public AvailabilityDialogController()
    {
        init();
    }
    public AvailabilityDialogController(Id thePractitionerId)
    {
        selectedPractitionerId = thePractitionerId;
        availabilityTypeOptions = new List<SelectOption>();
        availabilityTypeOptions.add(new SelectOption('0', 'REGULAR TIME TIME START & FINISH'));
        availabilityTypeOptions.add(new SelectOption('1', 'IRREGULAR TIME TIME START & FINISH'));
        init();
    }
    
    public void init()
    {
        availabilityType = '0';
        isSavedSuccessfully = false;
        slotsToDelete = new List<WorkingTimeSlot__c>();
        weekDaysRegular = new List<Boolean>{false, false, false, false, false, false, false};
        regularTS = new List<WorkingTimeSlot__c>{new WorkingTimeSlot__c(), new WorkingTimeSlot__c()};
        List<WorkingTimeSlot__c> aDBList = [SELECT Id, StartTime__c, EndTime__c, WeekDay__c, IsStandardWorkDay__c
             FROM WorkingTimeSlot__c WHERE Practitioner__c = :selectedPractitionerId ORDER BY WeekDay__c, StartTime__c];
        timeSlotMap = new Map<Integer, List<WorkingTimeSlot__c>>();
        weekDays = new List<Integer>();
        for (WorkingTimeSlot__c aSlot: aDBList)
        {
            if (regularTS[0].Id == null)
            {
                regularTS[0] = aSlot.clone();
                regularTS[0].Id = aSlot.Id;
            } else if (regularTS[1].Id == null && aSlot.WeekDay__c == regularTS[0].WeekDay__c)
            {
                regularTS[1] = aSlot.clone();
                regularTS[1].Id = aSlot.Id;
            }
            if (aSlot.IsStandardWorkDay__c != true)
            {
                availabilityType = '1';
            } else
            {
                weekDaysRegular[Integer.valueOf(aSlot.WeekDay__c)] = true;
            }
            List<WorkingTimeSlot__c> aSlots = timeSlotMap.get((Integer)aSlot.WeekDay__c);
            if (aSlots == null)
            {
                aSlots = new List<WorkingTimeSlot__c>();
                timeSlotMap.put((Integer) aSlot.WeekDay__c, aSlots);
            }
            if (aSlots.size() < 2)
            {
                aSlots.add(aSlot);
            } else
            {
                slotsToDelete.add(aSlot);
            }
        }
        for (Integer i = 0; i < 7; i++)
        {
            weekDays.add(i);
            List<WorkingTimeSlot__c> aSlots = timeSlotMap.get(i);
            if (aSlots == null)
            {
                aSlots = new List<WorkingTimeSlot__c>();
                timeSlotMap.put(i, aSlots);
            }
            if (aSlots.size() < 2)
            {
                Integer aCount = 2 - aSlots.size();
                for (Integer j = 0; j < aCount; j++)
                {
                    aSlots.add(new WorkingTimeSlot__c(WeekDay__c = i, Practitioner__c = selectedPractitionerId));
                }
            }
        }
    }
    
    public Boolean isValidForm()
    {
        Boolean aResult = true;
        if (availabilityType == '0')
        {
            aResult = validateTimeBlocks(regularTS[0], regularTS[1]);
        } else
        {
            for (Integer aDay: timeSlotMap.keySet())
            {
                List<WorkingTimeSlot__c> aSlots = timeSlotMap.get(aDay);
                aResult &= validateTimeBlocks(aSlots[0], aSlots[1]);
                if (aResult == false)
                {
                    return aResult;
                }
            }
        }
        return aResult;
    }
    
    private Boolean validateTimeBlocks(WorkingTimeSlot__c theSlot1, WorkingTimeSlot__c theSlot2)
    {
        Boolean aResult = validateTimeSlot(theSlot1);
        aResult &= validateTimeSlot(theSlot2);
        if (aResult)
        {
            Time aStartTime1 = Utils.parseTime(theSlot1.StartTime__c);
            Time aStartTime2 = Utils.parseTime(theSlot2.StartTime__c);
            Time anEndTime1 = Utils.parseTime(theSlot1.EndTime__c);
            Time anEndTime2 = Utils.parseTime(theSlot2.EndTime__c);
            DateTime aDTS1 = aStartTime1 ==  null ? null : Datetime.newInstance(Date.today(), aStartTime1);
            DateTime aDTS2 = aStartTime2 == null ? null : Datetime.newInstance(Date.today(), aStartTime2);
            DateTime aDTE1 = anEndTime1 == null ? null : Datetime.newInstance(Date.today(), anEndTime1);
            DateTime aDTE2 = anEndTime2 == null ? null : Datetime.newInstance(Date.today(), anEndTime2);
            if (aDTS1 != null && aDTS2 != null && aDTE1 != null && aDTE2 != null && aDTS1 <= aDTE2 && aDTE1 >= aDTS2)
            {
                theSlot2.addError('Time slots should not cross');
                aResult = false;
            }
        }
        return aResult;
    }
    
    private Boolean validateTimeSlot(WorkingTimeSlot__c theSlot)
    {
        Boolean aResult = true;
        Time aStartTime = null;
        Time anEndTime = null;
        try
        {
            aStartTime = Utils.parseTime(theSlot.StartTime__c);
            theSlot.StartTime__c = aStartTime == null ? null : String.valueOf(aStartTime).substring(0, 5);
        } catch (Exception theException)
        {
            theSlot.StartTime__c.addError(theException.getMessage());
            aResult = false;
        }
        try
        {
            anEndTime = Utils.parseTime(theSlot.EndTime__c);
            theSlot.EndTime__c = anEndTime == null ? null :String.valueOf(anEndTime).substring(0, 5);
        } catch (Exception theException)
        {
            theSlot.EndTime__c.addError(theException.getMessage());
            aResult = false;
        }
        if (aResult)
        {
            if ((aStartTime != null && anEndTime == null) || (aStartTime == null && anEndTime != null))
            {
                theSlot.addError('Start and End time must be set : ' + theSlot.StartTime__c + '/' + theSlot.EndTime__c);
                aResult = false;
            } else if (aStartTime != null && anEndTime != null
                    && Datetime.newInstance(Date.today(), aStartTime) >= Datetime.newInstance(Date.today(), anEndTime))
            {
                theSlot.addError('End Time must be after Start Time : ' + theSlot.EndTime__c);
                aResult = false;
            }
        }
        return aResult;
    }
    
    public void doSaveAvailability()
    {
        Boolean anIsValid = isValidForm();
        if (!anIsValid)
        {
            return;
        }
        List<WorkingTimeSlot__c> aSlotsToUpsert = new List<WorkingTimeSlot__c>();
        for (Integer aDay: timeSlotMap.keySet())
        {
            List<WorkingTimeSlot__c> aSlots = timeSlotMap.get(aDay);
            if (availabilityType == '0')
            {
                for (Integer anIndex = 0; anIndex < 2; anIndex ++)
                {
                    WorkingTimeSlot__c aSlot = aSlots.get(anIndex);
                    if (regularTS[anIndex].StartTime__c != null && regularTS[anIndex].EndTime__c != null
                            && regularTS[anIndex].StartTime__c != '' && regularTS[anIndex].EndTime__c != ''
                            && weekDaysRegular[aDay] == true)
                    {
                        aSlot.StartTime__c = regularTS[anIndex].StartTime__c;
                        aSlot.EndTime__c = regularTS[anIndex].EndTime__c;
                        aSlot.IsStandardWorkDay__c = true;
                        aSlotsToUpsert.add(aSlot);
                    } else if (aSlot.Id != null)
                    {
                        slotsToDelete.add(aSlot);
                    }
                }
            } else
            {
                for (WorkingTimeSlot__c aSlot: aSlots)
                {
                    if (aSlot.StartTime__c != null && aSlot.StartTime__c != ''
                            && aSlot.EndTime__c != null && aSlot.EndTime__c != '')
                    {
                        aSlot.IsStandardWorkDay__c = false;
                        aSlotsToUpsert.add(aSlot);
                    } else if (aSlot.Id != null)
                    {
                        slotsToDelete.add(aSlot);
                    }
                }
            }
        }
        upsert aSlotsToUpsert;
        delete slotsToDelete;
        isSavedSuccessfully = true;
    }*/
}