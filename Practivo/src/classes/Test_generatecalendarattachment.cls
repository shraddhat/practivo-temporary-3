@istest
public class Test_generatecalendarattachment 
{
  static testmethod void testcalattach()
    {
        Contact con =  new Contact();
        con.FirstName = 'Anilgsdfgsd';
        con.LastName = 'Dutt011';
        con.Email = 'anil@swiffgsdgtsestup.com';
        insert con;
        Contact con1 =  new Contact();
        con1.FirstName = 'Axnildfgsdfg';
        con1.LastName = 'Dxutt012';
        con1.Email = 'anil@xsdfgsdfgx.com';
        insert con1;
        Contact con2 =  new Contact();
        con2.FirstName = 'Axnilgdfg';
        con2.LastName = 'Dxutt0123';
        con2.Email = 'anil@xxsdfgsdg.com';
        insert con2;
       set <id> setconid= new set<id>();
        set <id> setconid2= new set<id>();
            setconid.add(con.id);
            setconid.add(con1.id);
            setconid2.add(con1.id);
            Attachment att=new Attachment();
            att.Body=Blob.valueof('sample');
            att.ContentType=String.valueof('text/calendar');
            att.Name=String.valueof('meeting.ics');
            att.description='Ical feed';
            att.parentId=con.Id;
            insert att;
        Event Eventdetails=new event();
        Eventdetails.subject='sample';
        Eventdetails.Event_Type__c='Appointment';
        Eventdetails.whoid=con.Id;
        Eventdetails.IsRecurrence=false;
        Eventdetails.StartDateTime=Datetime.valueof('2016-01-30 01:00:00');
        Eventdetails.EndDateTime=Datetime.valueof('2016-01-30 02:00:00');
        insert Eventdetails;
        EventRelation evrel=new EventRelation();
        evrel.EventId=Eventdetails.id;
        evrel.RelationId=con2.id;
        evrel.isparent=true;
        insert evrel;
            test.startTest();
        GenerateCalendarAttachment gcal=new GenerateCalendarAttachment(setconid);
         System.assertNotEquals(0, GenerateCalendarAttachment.listappointments.size());
        GenerateCalendarAttachment gcal1=new GenerateCalendarAttachment(setconid2);
            test.stopTest();
        System.assertNotEquals(null, GenerateCalendarAttachment.listappointments);
       
        list<Attachment> listcurrentattachment = [SELECT ID,name,body,ContentType,description,ParentID  FROM Attachment WHERE Description='Ical feed' and ParentID=:con.id];
        system.assertEquals(1, listcurrentattachment.size());
    }
}