@RestResource(urlMapping='/darshan/*') 
global without sharing class darshanWebserviceTest {
    @HttpPost
    global static void getAvailableCalendar()    
    {
        
       String callback,requestJson;
       map<string,string> fetchDataMap =  new map<String,string>(); 
       system.debug('dfsdf'+RestContext.request.params);
       for(string str :  RestContext.request.params.KeySet())
       {
       	fetchDataMap.put(str, RestContext.request.params.get(str));
       }
        system.debug('--callback--'+RestContext.request.requestBody.toString());
        //RestResponse httpResponse = RestContext.response;
        RestRequest httpRequest = RestContext.request;
        RestResponse httpResponse = RestContext.response;
        //string requestJson = RestContext.request.params.get('callback');
        system.debug('--requestJson--'+requestJson);
        
		/*list<Utils.jsonHelperClass> lst = new list<Utils.jsonHelperClass>();
        lst = (list<Utils.jsonHelperClass>)JSON.deserialize(requestJson,list<Utils.jsonHelperClass>.Class); 
        for(Utils.jsonHelperClass fetchObj : lst) 
        {	
        	fetchDataMap.put(fetchObj.name,fetchObj.value);
        	system.debug('--endUserWidgetAPI:getAvailableCalendar:RequestData (name)--'+fetchObj.name+'	(value)--'+fetchObj.value);
        }*/ 
        string returnValue = ScheduleWizardController.getResourceAvailCalendar(fetchDataMap.get('weekStart'),fetchDataMap.get('weekEnd'),fetchDataMap.get('locationId'),fetchDataMap.get('categoryId'),fetchDataMap.get('appointmentTypeId'),fetchDataMap.get('practitionerId'));
        system.debug('--endUserWidgetAPI:getAvailableCalendar:returnValue--'+returnValue);
		 //string returnValue='hello there';
		httpResponse.addHeader('Content-Type', 'application/json');
		//httpResponse.addHeader('Content-Type', 'application/javascript');
        httpResponse.responseBody = Blob.valueOf(returnValue);
        httpResponse.addHeader('Access-Control-Allow-Origin', '*');
        httpResponse.addHeader('Access-Control-Allow-Methods', '*');
		//httpResponse.responseBody = Blob.valueOf(returnValue);                  
    }
  public class jsonHelperWrapper
  {
     public list<jsonHelperClass> jsonData= new list<jsonHelperClass>();
   
  }
  public class jsonHelperClass
  {
    public string name;
    public string value;
    public jsonHelperClass(string key, string val)
    {
      this.name= key;
      this.value = val;
    }
  }
}