/*
***************************************
 class name-ViewWaitListController
 Class Description-Class for handling most of the waitlist related fetch operations.
 *************************************
*/
public class ViewWaitListController 
{
    public static list<event> listevent{get;set;}
    public static list<eventwrapper> listeventwrapper{get;set;}
    public static list<contact> listcontacts{get;set;}
    public string evId{get;set;}   
    public ViewWaitListController()
    {
        try
        {
            if(Utils.getReadAccessCheck('contact',new String []{'Id','name','Phone','Email','Salutation','RecordTypeId','PractitionerColor__c'}))
                listcontacts=[SELECT id,name,Phone,Email,Salutation,RecordTypeId,PractitionerColor__c,Resource_Type__r.Id,RecordType.developername FROM contact WHERE (Recordtype.developername=:String.escapeSingleQuotes(Utils.contact_CustomerRecordType) OR Recordtype.developername=:String.escapeSingleQuotes(Utils.contact_ServiceProviderRecordType) OR Recordtype.developername=:String.escapeSingleQuotes(Utils.contact_AssistantRecordType)) LIMIT 2000];
            else
                throw new Utils.ParsingException('No Read access to contact or Fields in contact.');
        }
        catch(Exception e)
        {

        }        
    }
    /*
     @MethodName : getFilteredeventwrapperlistjson
     @Return Type :  list<eventwrapper>  
     @Author : Koustubh Kulkarni.
     @Description : This method is used to fetch filtered waitlist based on the criterias of the provided events.
   */
    public Static list<eventwrapper> getFilteredeventwrapperlistjson(list<event> listofevents)
    {
        system.debug('inside');
        string jsonformattedlist;
        String str;
        try
        {
             listeventwrapper=new list<eventwrapper>();
            if(listofevents!=null)
            {
                for(event ev:listofevents)
                {
                    listeventwrapper.add(new eventwrapper(ev)); 
                }
            }
        }
        catch(exception ex)
        {
            System.debug('ViewWaitListController-->getFilteredeventwrapperlistjson--'+ex.getmessage()+ex.getstacktracestring());
        }
        /*str=JSON.serializePretty(listeventwrapper);
        system.debug('JSON String'+str);
        jsonformattedlist=String.escapeSingleQuotes(str);*/
        return listeventwrapper;
    }
    /*
     @MethodName : getFilteredeventwrapperlistjson
     @Return Type :  String (Serialized Json) 
     @Author : Koustubh Kulkarni.
     @Description : This method is used to fetch global waitlist.
   */
    public Static String geteventwrapperlistjson()
    {
        string str;
        string str1;
        try
        {
           if(Utils.getReadAccessCheck('Event',new String []{'Id','createddate','WhoId', 'Appointment_Type__c','Description__c','Location_Category_Mapping__c'}))
                {
                    listevent=[SELECT id,whoid,createddate,Description__c,Appointment_Type__r.name,
                        Location_Category_Mapping__r.Category__r.Name,Location_Category_Mapping__r.location__r.Name,(SELECT id,relationid,eventid FROM eventrelations) 
                        FROM event WHERE Event_Type__c=:String.escapeSingleQuotes(Utils.EventType_Waiting) and IsRecurrence=false LIMIT 2000];   
                }
            else
                throw new Utils.ParsingException('No Read access to Event or Fields in Event.');    
            listeventwrapper=new list<eventwrapper>();
            if(listevent!=null)
            {
                for(event ev:listevent)
                {
                    listeventwrapper.add(new eventwrapper(ev)); 
                }
            }
            str=JSON.serialize(listeventwrapper);
            str1=str.escapeHtml4();
            str1=String.escapeSingleQuotes(str1);
        }     
        catch(exception e)
        {
            system.debug('ViewWaitListController-->geteventwrapperlistjson--'+e.getmessage()+e.getstacktracestring());
        }      
     return str1;
    }
    public list<eventwrapper> geteventwrapperlist()
    {
        try
        {
            if(Utils.getReadAccessCheck('Event',new String []{'Id','createddate','WhoId', 'Appointment_Type__c','Description__c','Location_Category_Mapping__c'}))
                    listevent=[SELECT id,whoid,createddate,Description__c,Appointment_Type__r.name,Location_Category_Mapping__r.Category__r.Name,Location_Category_Mapping__r.location__r.Name FROM event WHERE Event_Type__c=:String.escapeSingleQuotes(Utils.EventType_Waiting) AND IsRecurrence=false LIMIT 2000];
            else
                 throw new Utils.ParsingException('No Read access to Event or Fields in Event.'); 
            listeventwrapper=new list<eventwrapper>(); 
            if(listevent!=null)
            {
                for(event ev:listevent)
                {
                    listeventwrapper.add(new eventwrapper(ev)); 
                }
            }
        }
        catch(exception e)
        {
            System.debug('ViewWaitListController-->geteventwrapperlist-->'+e.getmessage()+e.getstacktracestring());
        }
        
     return listeventwrapper;
    }
    
    public class eventwrapper
    {
        //This wrapper class contains all the fields which are necessary for displaying waitlist.
        public string eveid{get;set;}
        public string Name{get;set;}
        public string location{get;set;} 
        public string Category{get;set;} 
        public string Appointmenttype{get;set;}
        public string locationId{get;set;} 
        public string CategoryId{get;set;} 
        public string AppointmenttypeId{get;set;}
        public string ServiceproviderId{get;set;}
        public string addeddate{get;set;}  
        public string Des{get;set;}
        public string relid{get;set;}
        public string relname{get;set;}
        public string relconno{get;set;}
        public string relemail{get;set;}
        public eventwrapper(event ev)
        {
            System.debug('EV-->'+ev);
            contact ConRecSP=getcontact(ev.whoid);
            string whoid=ev.whoid;
            this.eveid=ev.id;
            if(ConRecSP.RecordType.developername==Utils.contact_CustomerRecordType)
            {
                this.Name='Any';
                this.ServiceproviderId='';
            }
            else if(ConRecSP.RecordType.developername==Utils.contact_ServiceProviderRecordType)
            {
                this.Name=ConRecSP.Salutation+ConRecSP.Name;
                this.ServiceproviderId=ConRecSP.Id;
            }
            this.location=ev.Location_Category_Mapping__r.location__r.Name;
            this.locationId=ev.Location_Category_Mapping__r.location__r.Id;
            this.Category=ev.Location_Category_Mapping__r.Category__r.Name;
            this.CategoryId=ev.Location_Category_Mapping__r.Category__r.Id;
            this.AppointmenttypeId=ev.Appointment_Type__r.Id;
            this.Appointmenttype=ev.Appointment_Type__r.Name;
            this.addeddate=getdate(ev.createddate);
            this.Des=ev.Description__c;
            Boolean checkForAnyCase;
            /*set<id> seteventids=(new Map<Id,SObject>(newEvents)).keySet();
       set<id> setevwhoids=new set<id>();
       For (Event e1 : newEvents)
       {
          if(e1.whoid !=null)
          {
            setevwhoids.add(e1.whoid);
          }
       }*/
            if(ev.eventrelations.size()>1)
            {
                checkForAnyCase=false;
            }
            else
            {
                checkForAnyCase=true;
            }
            try{
                 for(EVENTrelation evr:ev.eventrelations)
                {
                    if(evr.eventid==ev.id && evr.relationid!=ev.whoid && checkForAnyCase==false)
                    {    
                        contact ConRecCust=getcontact(evr.relationid);
                        this.relid=evr.relationid;  
                        this.relname=ConRecCust.Name;
                        this.relconno=ConRecCust.Phone;
                        this.relemail=ConRecCust.Email;
                    }
                     if(evr.eventid==ev.id && checkForAnyCase==true )
                    {
                        contact ConRecCust=getcontact(evr.relationid);
                        this.relid=evr.relationid;  
                        this.relname=ConRecCust.Name;
                        this.relconno=ConRecCust.Phone;
                        this.relemail=ConRecCust.Email;
                    }
                }
                }
            catch(exception e)
            {
                e.getmessage();
            }   
        }
        public string getdate(datetime cdate)
        {
            Date myDate = cdate.Date();
            String StrmyDate=String.valueof(myDate);
             return StrmyDate;
        }
        public contact getcontact(id relid)
        {
            contact cont=new contact();
            try
            {
                System.debug('listcontacts'+listcontacts);
                for(contact con:listcontacts)
                {
                    if(con.id==relid)
                    {
                        cont=con; 
                        break;  
                    }
                }
            }
            catch(exception ex)
            {
               system.debug('getcontact'+ex.getmessage()+ex.getstacktracestring());
            }
            return cont;
        }
    }

}