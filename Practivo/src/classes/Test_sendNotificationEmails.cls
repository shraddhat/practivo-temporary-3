@istest
public class Test_sendNotificationEmails 
{
    static testmethod void testEmailnotifiaction()
    {
        contact Sp=TestDataUtils.getServiceProvider();
        contact cust=TestDataUtils.getCustomer();
        Location__c loc=TestDataUtils.getlocation();
        Category__c cat=TestDataUtils.getcategory();
        AppointmentType__c apt=TestDataUtils.getappointment(cat);
        Location_Category_Mapping__c locmap=TestDataUtils.getlocationmapping(loc, cat, Sp);
        Resource_Type__c rtyp=TestDataUtils.getResourceTypeNH();
        Resources__c res=TestDataUtils.getresources(loc, rtyp); 
        Booked_Resources__c book=TestDataUtils.getbookedresources();
        Actual_Resource_Booked_Mapping__c aResbookmap=TestDataUtils.getactualbooked(res, book);
        Event ev=TestDataUtils.geteventappointmentwithall(Sp,locmap,apt);
        EventRelation evrel=TestDataUtils.geteventrelation(ev, cust);
        Event ev1=TestDataUtils.geteventappointmentwithall(Sp,locmap,apt);
        ev1.Event_Type__c='Partial';
        update ev1;
        EventRelation evrel1=TestDataUtils.geteventrelation(ev1, cust);
        Event ev2=TestDataUtils.geteventappointmentwithall(Sp,locmap,apt);
        ev2.Event_Type__c='Cancelled';
        ev2.whatid=book.id;
        update ev2;
        EventRelation evrel2=TestDataUtils.geteventrelation(ev2, cust);
        set<id> lstevs=new set<id>();
        lstevs.add(ev.id);
        lstevs.add(ev1.id);
        lstevs.add(ev2.id);
        test.startTest();
        SendNotificationEmails sn=new SendNotificationEmails();
        boolean result=SendNotificationEmails.SendEmail(lstevs,true,false);
        SendNotificationEmails.SendEmailtoSinglepatient(ev1.id, cust.id, true);
       system.assertEquals(false, result);
        test.stopTest();
    }
    static testmethod void testEmailnotifiactionNegative()
    {
        contact Sp=TestDataUtils.getServiceProvider();
        contact cust=TestDataUtils.getCustomer();
        Location__c loc=TestDataUtils.getlocation();
        Category__c cat=TestDataUtils.getcategory();
        AppointmentType__c apt=TestDataUtils.getappointment(cat);
        Location_Category_Mapping__c locmap=TestDataUtils.getlocationmapping(loc, cat, Sp);
        Resource_Type__c rtyp=TestDataUtils.getResourceTypeNH();
        Resources__c res=TestDataUtils.getresources(loc, rtyp); 
        Booked_Resources__c book=TestDataUtils.getbookedresources();
        Actual_Resource_Booked_Mapping__c aResbookmap=TestDataUtils.getactualbooked(res, book);
        Event ev=TestDataUtils.geteventappointmentwithall(Sp,locmap,apt);
        EventRelation evrel=TestDataUtils.geteventrelation(ev, cust);
        Event ev1=TestDataUtils.geteventappointmentwithall(Sp,locmap,apt);
        ev1.Event_Type__c='Partial';
        update ev1;
        EventRelation evrel1=TestDataUtils.geteventrelation(ev1, cust);
        Event ev2=TestDataUtils.geteventappointmentwithall(Sp,locmap,apt);
        ev2.Event_Type__c='Cancelled';
        ev2.whatid=book.id;
        update ev2;
        EventRelation evrel2=TestDataUtils.geteventrelation(ev2, cust);
        set<id> lstevs=new set<id>();
        lstevs.add(ev.whatid);
        lstevs.add(ev1.whoid);
        lstevs.add(ev2.id);
        test.startTest();
        SendNotificationEmails sn=new SendNotificationEmails();
        boolean result=SendNotificationEmails.SendEmail(lstevs,true,false);
        SendNotificationEmails.SendEmailtoSinglepatient(ev1.id, cust.id, false);
        SendNotificationEmails.sendEmailForExportCalendar(Sp.id,Sp.id,false,'Error','Stacktrace');
        //system.assertEquals(false, result);
        test.stopTest();
    }
    static testmethod void testEmailnotifiactionupdate()
    {
        contact Sp=TestDataUtils.getServiceProvider();
        contact cust=TestDataUtils.getCustomer();
        Location__c loc=TestDataUtils.getlocation();
        Category__c cat=TestDataUtils.getcategory();
        AppointmentType__c apt=TestDataUtils.getappointment(cat);
        Location_Category_Mapping__c locmap=TestDataUtils.getlocationmapping(loc, cat, Sp);
        Resource_Type__c rtyp=TestDataUtils.getResourceTypeNH();
        Resources__c res=TestDataUtils.getresources(loc, rtyp); 
        Booked_Resources__c book=TestDataUtils.getbookedresources();
        Actual_Resource_Booked_Mapping__c aResbookmap=TestDataUtils.getactualbooked(res, book);
        Event ev=TestDataUtils.geteventappointmentwithall(Sp,locmap,apt);
        EventRelation evrel=TestDataUtils.geteventrelation(ev, cust);
        Event ev1=TestDataUtils.geteventappointmentwithall(Sp,locmap,apt);
        ev1.Event_Type__c='Partial';
        update ev1;
        EventRelation evrel1=TestDataUtils.geteventrelation(ev1, cust);
        Event ev2=TestDataUtils.geteventappointmentwithall(Sp,locmap,apt);
        ev2.Event_Type__c='Cancelled';
        ev2.whatid=book.id;
        update ev2;
        EventRelation evrel2=TestDataUtils.geteventrelation(ev2, cust);
        set<id> lstevs=new set<id>();
        lstevs.add(ev.id);
        lstevs.add(ev1.id);
        lstevs.add(ev2.id);
        test.startTest();
        SendNotificationEmails sn=new SendNotificationEmails();
        boolean result=SendNotificationEmails.SendEmail(lstevs,true,true);
        SendNotificationEmails.SendEmail(lstevs,true,false);
        System.assertEquals(false, result);
        test.stopTest();
    }
}