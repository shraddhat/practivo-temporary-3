Public class Map_assistant{
    
    @RemoteAction
    Public Static String AddWrapper() 
    {   
        
        string str;
        string str1;
        List<Location__c> LocList = [Select Id,name From Location__c];
        List<Category__c> ListCat = [select Id , Name from Category__c];
        
        List<Contact> ConList = [Select Id , Name,RecordTypeId from Contact where RecordTypeId = '01228000000dZrA' or RecordTypeId = '01228000000fm5p'];
        
        List<Location_Category_Mapping__c> LocMapList = [Select Id , Name , Contact__r.id , Location__r.id , Category__r.id from Location_Category_Mapping__c];
        List<WrapperClass> ListWrap=new list<WrapperClass>();
        
    ListWrap.add(new WrapperClass(LocList,ConList,ListCat,LocMapList ));
    
    str=JSON.serialize(new WrapperClass(LocList,ConList,ListCat,LocMapList ));
        str1=String.escapeSingleQuotes(str);
     System.debug('The value is: ' + str1);
     return str1;
     
    }
    public class requestWrapper {
            string LocCatMapId;
            string LocCatLocId; 
            string LocCatConId;
            string LocCatCatId;
        }  
  /*  public class saveMappingTopWrapper {
        List<requestWrapper> WCloccatmaplist = new List<requestWrapper>();
    }
    @RemoteAction
    public static boolean saveMapping(string requestData) {   
        try {
             saveMappingTopWrapper mappingBody = new saveMappingTopWrapper();
            mappingBody = (saveMappingTopWrapper)JSON.deserialize(requestData, saveMappingTopWrapper.Class);        
            system.debug('mappingBody==>'+mappingBody);
            List<Location_Category_Mapping__c> mappingList = new List<Location_Category_Mapping__c>();
            for(requestWrapper locationMapping : mappingBody.WCloccatmaplist) {
                Location_Category_Mapping__c obj = new Location_Category_Mapping__c();    
                obj.id = locationMapping.LocCatMapId;
                obj.Category__c = locationMapping.LocCatCatId;
                obj.Contact__c = locationMapping.LocCatConId;
                obj.Location__c = locationMapping.LocCatLocId;
                mappingList.add(obj);
            }
            upsert mappingList;
            return true;
        }
        catch(Exception ex) {
            return false;
        }
       
    }*/
    //wrapper class
    public class WrapperClass 
    {
        public list<WrapperClassTwo> WCloccatmaplist{get;set;}
        Public List<WrapperClassThree> WContactlist{get;set;}
         Public List<WrapperClassThree> WCategorylist{get;set;}
         Public List<WrapperClassThree> WLocationlist{get;set;}
    
    public WrapperClass(List<Location__c> Wraploc, List<Contact> WrapCont ,List<Category__c> WrapCat , List<Location_Category_Mapping__c>WrapLocMap ) 
    {
        WLocationlist = new List<WrapperClassThree>();
        for(Location__c Loc : Wraploc)
        {
          WLocationlist.add(new WrapperClassThree(Loc));
       }
       
       
     WCategorylist = new List<WrapperClassThree>();
        for(Category__c Cat : WrapCat)
        {
          WCategorylist.add(new WrapperClassThree(Cat));
       }
        
        WContactlist = new List<WrapperClassThree>();
        for(Contact Con : WrapCont)
        {
          WContactlist.add(new WrapperClassThree(con));
       }
        
        WCloccatmaplist = new List<WrapperClassTwo>();
        for(Location_Category_Mapping__c lcm : WrapLocMap)
        {
            WCloccatmaplist.add(new WrapperClassTwo(lcm));
        }
    }
        
    }     
    
    Public Class WrapperClassTwo{
        
        public Id LocCatMapId{get;set;}
        public Id LocCatLocId{get;set;}
        public Id LocCatConId{get;set;}
        public Id LocCatCatId{get;set;}

        Public WrapperClassTwo(Location_Category_Mapping__c Cot){
            
            this.LocCatMapId = cot.id;
            this.LocCatLocId = cot.Location__r.Id;
            This.LocCatConId = cot.Contact__r.Id;
            This.LocCatCatId = cot.Category__r.Id;
        }
        }
        Public Class WrapperClassThree
        {	
            public String Name{get;set;}
			public Id recId{get;set;}
			public Boolean bol{get;set;}
            
            Public WrapperClassThree(Location__c loct)
            {
                this.Name= loct.Name;
				this.recid=loct.Id;
                bol = false;
                
            }
          Public WrapperClassThree(Contact Cont)
          {
              	this.Name=Cont.Name;
				this.recid=Cont.Id;
              //For practicioner
              if(Cont.RecordTypeId == '01228000000dZrA')
				{
					this.bol = false;
				}
              //For assistant
              if(Cont.RecordTypeId == '01228000000fm5p')
              {
                  this.bol = true;
              }
          }
          Public WrapperClassThree(Category__c Catg)
          {
            this.Name=Catg.Name;
			this.recid=Catg.Id;
             bol = false; 
              
              
              
          }
          
    }
}